/**
 * Daniel Rosenthal
 * ID: 108724857
 * Homework #1
 * CSE Recitation Section: 5
 * @author Daniel
 */
import java.util.Scanner;
public class Stackotaire {
	//This class contains a main method which allows the user to play a game of solitaire using text inputs
	//the move command should be in format "move t5 t2" (moves from stack t5 to stack t2)
	//the moveN command should be in format "moven t2 t3 5" and moves 5 cards
	private static CardStack deck;//the stack for the main deck
	private static CardStack[] tableau = new CardStack[8];//array of stacks for tableau tableau[0] is null
	private static CardStack[] foundations = new CardStack[5];//array of stacks for foundations foundations[0] is null
	private static CardStack  waste;//stack for the waste pile
	private static Card[] list;//shallow copy of all the cards in the deck
	private static boolean repeat=true;//when false program terminates
	private static Scanner input = new Scanner(System.in);//scanner for program
	private static String command;//string representing user input
	
	/*
	 * main method: displays game board, prompts user for input, refreshes after input
	 */
	public static void main(String[] args){
		restart();
		while (repeat){
			display();
			
			command=input.next();
			switch(command){
			
			case("Draw"):
			case("draw"):
				draw();
			break;
				
			case("Move"):
			case("move"):
				move(input.next(),input.next());
			break;
				
			case("MoveN"):
			case("Moven"):
			case("moveN"):
			case("moven"):
				moveN(input.next(),input.next(),input.nextInt());
			break;
			
			case("Win"):
			case("win"):
				win();
			break;
			
			case("Restart"):
			case("restart"):
				restart();
			break;
			
			case("Quit"):
			case("quit"):
				System.out.println("Thank you for Playing!");
				repeat=false;
			break;
			
			default:
				System.out.println("Invalid input");
			}
			input.nextLine();
		}
	}
	/*
	 * prints the game board and command instructions
	 */
	public static void display() {
		for (int i=1;i<5;i++)
			System.out.print("F"+i+foundations[i].printStack());
		
		System.out.println("\t W1"+waste.printStack()+"\t"+deck.printStack()+deck.size());
		
		for (int i=1;i<8;i++)
			System.out.println("T"+i+tableau[i].printStack());
		
		System.out.println("Commands: Draw, Move, MoveN, Win, Restart, Quit");
	}

	/*
	 * draws a card from the main deck into the waste pile and turns it face up
	 * if the deck is empty: places waste back in deck face down, if waste is empty prints invalid move message
	 */
	public static void draw(){
		if(deck.isEmpty()){
			if(waste.isEmpty())
				System.out.println("Invalid move, deck is empty");
			else{
				while(!waste.isEmpty()){
					waste.peek().setFaceUp(false);
					deck.push(waste.pop());
				}
			}
		}
		deck.peek().setFaceUp(true);
		waste.push(deck.pop());
	}
	
	public static void move(String from, String to){
		CardStack temp1=null;
		CardStack temp2=null;
		int pos1 =Character.getNumericValue(from.charAt(1));
		int pos2 =Character.getNumericValue(to.charAt(1));
		boolean invalid=false;
		switch(from.charAt(0)){
			case('T'):
			case('t'):
				temp1=tableau[pos1];
			break;
			
			case('W'):
			case('w'):
				temp1=waste;
			break;
			
			case('F'):
			case('f'):
				temp1=foundations[pos1];
			break;
			default:
				invalid=true;
		}
		if(temp1.isEmpty())
			invalid=true;
		else{
			switch(to.charAt(0)){
			case('T'):
			case('t'):
				temp2=tableau[pos2];
				if(temp2.isEmpty())
					invalid = (temp1.peek().getNum()!=13);//if card to move is king =false, else =true
				else if(temp2.peek().isRed()==temp1.peek().isRed() ||
						temp2.peek().getNum()!=temp1.peek().getNum()+1)
					invalid = true;
			break;
		
			case('W'):
			case('w'):
				invalid = true;
			break;
		
			case('F'):
			case('f'):
				temp2=foundations[pos2];
				if(temp2.isEmpty())
					invalid=(temp1.peek().getNum()!=1);//if card to move is ace =false, else =true
				else
					invalid=(temp2.peek().getSuit()!=temp2.peek().getSuit()
							|| temp2.peek().getNum()!=temp1.peek().getNum()-1);
			break;
			default:
				invalid=true;
			}
			if(invalid)
				System.out.println("Invalid move");
			else{
				
				temp2.push(temp1.pop());
				if(!temp1.isEmpty())
					temp1.peek().setFaceUp(true);
			}
		}
	}
	/*
	 * starts game fresh:
	 * creates new deck and shuffles it 
	 * creates new list containing shallow copy of all cards
	 * creates new foundations and tableau
	 * places cards from deck into initial stacks
	 */
	public static void restart() {
		deck = new CardStack('s');
		list = new Card[52];
		int counter=0;
		for(int i=1;i<5;i++){//adds each card to the deck and list
			for(int j=1;j<14;j++){
				deck.push(new Card(j,i));
				list[counter]=deck.peek();
				counter++;
			}
		}
		deck.shuffle();
		waste= new CardStack('w');
		for (int i=1;i<5;i++){
			foundations[i]= new CardStack('f');
		}
		for (int i=1;i<8;i++){
			tableau[i]= new CardStack('t');
		}
		for (int i=1;i<8;i++){
			deck.peek().setFaceUp(true);
			for (int j=i;j<8;j++){
				tableau[j].push(deck.pop());
			}
		}
		deck.peek().setFaceUp(true);
		waste.push(deck.pop());
			
	}
	/*
	 * moves a specified number of cards from one tableau to another
	 * prints invalid move message if move is invalid
	 * @param from: 2 char string representing the tableau cards are moving from
	 * @param to: 2 char string representing the tableau cards are moving to
	 * @param amount: the number of cards being moved
	 */
	public static void moveN(String from, String to, int amount){
		CardStack temp1=null;
		CardStack temp2=null;
		CardStack tempStack=new CardStack('s');
		int pos1 =Character.getNumericValue(from.charAt(1));
		int pos2 =Character.getNumericValue(to.charAt(1));
		boolean invalid=false;
		if(amount<1)
			invalid=true;
		else{
			switch(from.charAt(0)){
				case('T'):
				case('t'):
					temp1=tableau[pos1];
				break;
				default:
					invalid=true;
			}
			if(temp1.isEmpty() || temp1.size()<amount)
				invalid=true;
			else{
				while(amount>0 && temp1.peek().isFaceUp()){
					tempStack.push(temp1.pop());
					amount--;
				}
				if(amount>0)
					invalid=true;
				
				switch(to.charAt(0)){
				case('T'):
				case('t'):
					temp2=tableau[pos2];
					if(temp2.isEmpty())
						invalid = (tempStack.peek().getNum()!=13);//if first card to move is king =false, else =true
					else if(temp2.peek().isRed()==tempStack.peek().isRed() ||
							temp2.peek().getNum()!=tempStack.peek().getNum()+1)
						invalid = true;
					break;
				default:
					invalid=true;
				}
			}		
			if(invalid)
				System.out.println("Invalid move");
			else{
				while(!tempStack.isEmpty())
					temp2.push(tempStack.pop());
				
				if(!temp1.isEmpty())
					temp1.peek().setFaceUp(true);
			}
		}
	}
	/*
	 *checks if win condition has been achieved(all cards are either face up or in the deck)
	 */
	public static void win(){
		boolean win=true;
		int deckSize=deck.size();
		while(!deck.isEmpty())
			draw();
		for(Card card: list){
			if(!card.isFaceUp())
				win=false;
		}
		if(win){
			System.out.println("YOU'VE WON!!!");
			System.out.println("Play Again?(Yes/No)");
			command = input.nextLine();
			if (command=="No" || command=="no")
				repeat=false;
			else{
				System.out.println("Restarting");
				restart();
			}
		}
		else{
			System.out.println("You have not yet won");
			while(deck.size()!=deckSize){
				draw();
			}
			
		}
	}

}
	

	
