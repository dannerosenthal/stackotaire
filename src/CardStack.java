/**
 * Daniel Rosenthal
 * ID: 108724857
 * Homework #3
 * CSE Recitation Section: 5
 * @author Daniel
 */
import java.util.Stack;
import java.util.Collections;
public class CardStack {
	//This class is a stack of cards with a type that can be used to determine behavior
	private Stack<Card> cards; //Card stack
	private char stackType;//type of stack stored as a char
	
	/*
	 * constructor method
	 * @param type: type of Card stack
	 */
	public CardStack(char type){
		cards = new Stack<Card>();
		stackType=type;
	}
	/*
	 * calls push, pop, peek methods for stack
	 */
	public void push(Card newCard){
		cards.push(newCard);
	}
	
	public Card pop(){
		return cards.pop();
		
	}
	
	public Card peek(){
		return cards.peek();
	}
	/*
	 * @returns size of stack
	 */
	public int size(){
		return cards.size();
	}
	/*
	 * @returns true if stack is empty
	 */
	public boolean isEmpty(){
		return cards.isEmpty();
	}
	/*
	 * @returns string representation of stack, how the string is presented changes based on the type of stack
	 * returns empty brackets if stack is empty
	 */
	public String printStack(){
		if(isEmpty())
			return"[  ]";
		else{
			switch(stackType){
				case('s'):
				case('w'):
				case('f'):
					return peek().toString();
				case('t'):
					Stack<Card> temp = new Stack<Card>();
					String stackString="";
					while(!isEmpty()){
						temp.push(pop());
					}
					while(!temp.isEmpty()){
						stackString=stackString+temp.peek().toString();
						push(temp.pop());
					}
					return stackString;
				default:
					return null;
			}
		}
	}
	/*
	 * shuffles stack into random order
	 */
	public void shuffle(){
		Collections.shuffle(cards);
	}
	
	

}
