/**
 * Daniel Rosenthal
 * ID: 108724857
 * Homework #3
 * CSE Recitation Section: 5
 * @author Daniel
 */
public class Card {
	//this class is a standard playing card with a number/face, suit, and a value for face up or face down
	
	private boolean isRed, isFaceUp; //determine whether card is red or black and face up or face down
	private String value; //String containing the card number or face(Ace, King, Queen, Jack)
	private char suit; //character that represents the suit of the card
	private int number;//integer value of card number Ace=1, Jack=11, Queen=12, King=13
	private String values[] = {" ","A","2","3","4","5","6","7","8","9","10","J","Q","K"}; //array containing Strings of card value
    private char suits[]    = {' ', '\u2666', '\u2663','\u2665', '\u2660'};   //unicode for suits char {' ', 'diamond', 'club','heart', 'spade'}
	
    /*
     * constructor for a Card, determines color(isRed) based on suit value, default is face down for a card
     * @param cardNum the value of the card as an int
     * @param suitNum int corresponding to a suit in the suits[] array
     */
	Card(int cardNum, int suitNum){
		number=cardNum;
		suit=suits[suitNum];
		value=values[cardNum];
		isRed=(suitNum==1 || suitNum==3);
		isFaceUp=false;
	}
	
	/*
	 * set and get methods for parameters
	 */
	public int getNum(){
		return number;
	}
	public void setSuit(int suitNum){
		suit=suits[suitNum];
	}
	public char getSuit(){
		return suit;
	}
	public void setValue(String valueStr){
		value=valueStr;
	}
	public String getValue(){
		return value;
	}
	public void setFaceUp(boolean FaceUp){
		isFaceUp=FaceUp;
	}
	public boolean isFaceUp(){
		return isFaceUp;
	}
	public boolean isRed(){
		return isRed;
	}
	/*
	 *@returns string representation of card
	 *returns back of card if the card is facedown
	 */
	public String toString(){
		if(isFaceUp)
			return "[" + value+suit + "]";
		else
			return "[XX]";
	}
}

